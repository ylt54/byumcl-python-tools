.. byumcl documentation master file, created by

Welcome to byumcl's documentation!
==================================

.. module:: byumcl

**Date**: |today| **Version**: |version|

**Source Repository:** https://bitbucket.org/byumcl/byumcl

Welcome to the documentation for the byumcl python library. It will be epic.

Contents:

.. toctree::
    :maxdepth: 3

    install
    testfile


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

