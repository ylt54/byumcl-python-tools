===================
BYUMCL Python Tools
===================

Brigham Young University Macroeconomics and Computational Laboratory python library. Contains python routines for solving various DSGE models as well as other numerical tools used in economics.

If you are new to git, click on the wiki page and follow the link to the getting started with git page.


Modules
=======

* interpolate
* uhlig
* misc
* matstat
* partools

Contributors
============
- Jeremy Bejarano
- Chase Coleman
- Spencer Lyon
- Kerk Phillips
- Roy Roth
- Yulong Li