import numpy
from mpi4py import MPI
from byumcl import partools

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

N = 11

my_start, my_end, my_N = partools.ordered_balance(rank, size, N)
gv = partools.gatv_scatv_tuples(size, N)

if rank == 0:
    x = numpy.linspace(0, (N - 1) * 10, N)
    print "x = ", x
else:
    x = None

xlocal = numpy.zeros(my_N)

partools.rprint(comm, "Scatter")

comm.Scatterv([x, gv[0], gv[1], MPI.DOUBLE], xlocal)
partools.par_print(comm, "process " + str(rank) + " has " + str(xlocal))

comm.Barrier()
if rank == 0:
    print "Gather"
    xGathered = numpy.zeros(N)
else:
    xGathered = None

comm.Gatherv(xlocal, [xGathered, gv[0], gv[1], MPI.DOUBLE])
partools.par_print(comm, "process " + str(rank) + " has " + str(xGathered))

#Notice what happens if you switch comm.Gatherv to comm.Allgatherv, it will not
#immediately work. You will get a segmentation fault. That is because, as you
#will notice above, xGathered is only properly allocated on process 0.

#We could modify the code to use Allgatherv if we define xGathered on all
#processes, the same way we did on process 0.
