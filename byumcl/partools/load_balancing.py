#load_balancing.py
"""Tools to aid in the load balancing process.

This module contains tools to aid in the load balancing of parallel code.

By Jeremy Bejarano
"""

try:
    from mpi4py import MPI
except:
    print('Cannot find mpi4py. partools will not work')




def ordered_balance(rank, size, num_elements):
    """ordered_balance(rank, size, num_elements)

    Takes number of elements and returns a balanced division.

    Takes in num_elements, the proc's rank, the comm size, and returns the
    start and end of the list of elements that should be computed by process
    with rank, 'rank', as well as the number of elements given to each process.

    Examples:

    0,2,2 = ordered_balance(0,3,5)
    2,4,2 = ordered_balance(1,3,5)
    4,5,1 = ordered_balance(2,3,5)

    or

    0,2,2 = ordered_balance(0,3,4)
    2,3,1 = ordered_balance(1,3,4)
    3,4,1 = ordered_balance(2,3,4)
    """

    remainder = int(num_elements % size)

    #calulate distribution
    if (rank < remainder):
        my_num_elements = (num_elements//size) + 1
        my_start = rank * (num_elements//size) + rank
        my_end = my_start+my_num_elements
    else:
        my_num_elements = num_elements//size
        my_start = rank * (num_elements//size) + remainder
        my_end = my_start+my_num_elements

    my_N = my_end - my_start

    return my_start, my_end, my_N


def gatv_scatv_tuples(size, N):
    """gatv_scatv_tuples(size, N)

    Generate tuple parameters that are used in gatherv and scatterv, etc.

    Gatherv and Scatterv (and similar MPI functions) use tuple parameters for
    send_counts and displacements. This function generates those tuples.

    Examples:

    In the case of a comm with size=3 and 5 elements
    (2,2,1),(0,2,4) = gatv_scatv_tuples(3,5)

    In the case of a comm with size=3 and 4 elements
    (2,1,1),(0,2,3) = gatv_scatv_tuples(3,4)

    For use in a case like this.

    gv_tuples = gatv_scatv_tuples(3,5)
    comm.Allgatherv(v_local,[v,gv_tuples[0],gv_tuples[1],MPI.DOUBLE])
    """

    #create tuples that will be the parameters for Allgatherv
    gv_tuples = map(ordered_balance, range(size), [size] * size, [N] * size)
    #this creates three tuples, each with length "size": (1) my_starts,
    #(2) my_ends, and (3) my_Ns
    gv_tuples = zip(*gv_tuples)

    #return send_counts, displacements
    return gv_tuples[2], gv_tuples[0]



